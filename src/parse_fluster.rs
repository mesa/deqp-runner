use crate::parse::{ParserState, ResultParser};
use crate::TestStatus;
use anyhow::Result;
use regex::Regex;

impl TestStatus {
    pub fn from_fluster_str(input: &str) -> Result<TestStatus> {
        match input {
            "Success" => Ok(TestStatus::Pass),
            "Fail" => Ok(TestStatus::Fail),
            "Not Run" => Ok(TestStatus::Skip),
            "Error" => Ok(TestStatus::Crash),
            "Timeout" => Ok(TestStatus::Timeout),
            _ => anyhow::bail!("unknown fluster status '{}'", input),
        }
    }
}

pub struct FlusterResultParser {}

impl FlusterResultParser {
    pub fn new() -> FlusterResultParser {
        FlusterResultParser {}
    }
}

impl Default for FlusterResultParser {
    fn default() -> Self {
        Self::new()
    }
}

impl ResultParser for FlusterResultParser {
    fn initialize(&mut self) -> Option<ParserState> {
        None
    }

    fn parse_line(&mut self, line: &str) -> Result<Option<ParserState>> {
        let test_result_re =
            Regex::new(r"^\[.*\] \((?P<decoder>[^ ]*)\) (?P<test>[^ ]*) *\.\.\. (?P<result>.*)$")?;

        if let Some(cap) = test_result_re.captures(line) {
            return Ok(Some(ParserState::FullTest(
                format!("{}@{}", &cap["decoder"], &cap["test"]),
                TestStatus::from_fluster_str(&cap["result"])?,
            )));
        }

        Ok(None)
    }
}
