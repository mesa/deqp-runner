variables:
  RUST_BACKTRACE: "FULL"
  FDO_UPSTREAM_REPO: mesa/deqp-runner
  # Package version can only contain numbers (0-9), and dots (.).
  # Must be in the format of X.Y.Z, i.e. should match /\A\d+\.\d+\.\d+\z/ regular expresion.
  # See https://docs.gitlab.com/ee/user/packages/generic_packages/#publish-a-package-file
  PACKAGE_VERSION: "0.20.3"
  PACKAGE_REGISTRY_URL: "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/deqp-runner/${PACKAGE_VERSION}"
  AMD64_DEB: "deqp-runner_${PACKAGE_VERSION}_amd64.deb"
  ARM32_DEB: "deqp-runner_${PACKAGE_VERSION}_armhf.deb"
  ARM64_DEB: "deqp-runner_${PACKAGE_VERSION}_arm64.deb"

workflow:
  rules:
    # Do not duplicate pipelines on merge pipelines
    - if: $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS && $CI_PIPELINE_SOURCE == "push"
      when: never
    # Pushing to a fork
    - if: &fork $CI_PROJECT_PATH != $FDO_UPSTREAM_REPO
    # Pushing to a merge request
    - if: &merge-request $CI_PIPELINE_SOURCE == "merge_request_event"
    # Running after merging (immediately after, or scheduled)
    - if: &merged $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    # Pushing a tag
    - if: &tag $CI_COMMIT_TAG

default:
  before_script:
    - export PATH="$HOME/.cargo/bin:$PATH"

stages:
  - container
  - build
  - upload
  - release

test:
  rules:
    - if: *fork
    - if: *merge-request
  stage: build
  image: "rust:1.72.0-buster"
  script:
    # Make sure PACKAGE_VERSION gets bumped with Cargo.toml.
    - grep -q "version = .$PACKAGE_VERSION" Cargo.toml
    - rustup component add rustfmt clippy
    - cargo test -j ${FDO_CI_CONCURRENT:-4}
    - cargo fmt -- --check
    - cargo clippy -- --deny warnings
  needs: []

deb:
  rules:
    - if: *merged
    - if: *tag
  stage: build
  image: "rust:1.72.0-buster"
  script:
    - cargo install cargo-deb --version 1.44.1
    - cargo deb
  artifacts:
    paths:
      - target/debian/*.deb
  needs: []

deb-arm64:
  extends: deb
  image: "arm64v8/rust:1.72.0-buster"
  tags:
    - aarch64

deb-armhf:
  extends: deb-arm64
  image: "arm32v7/rust:1.72.0-buster"
  script:
    - cargo install cargo-deb --version 1.44.1
    - cargo deb -Z gz # fails with "cargo-deb: lzma compression error: Mem" without the -Z

test-stable:
  rules:
    - if: *fork
    - if: *merge-request
  stage: build
  image: "rust:1.66.0-buster"
  script:
    # Integration tests require newer compiler features so we can only build.
    - cargo build -j ${FDO_CI_CONCURRENT:-4}
  needs: []

.windows_docker:
  variables:
    DEQP_RUNNER_IMAGE_PATH: "windows/x64"
    DEQP_RUNNER_IMAGE_TAG: "2022-10-14"
    DEQP_RUNNER_IMAGE: "$CI_REGISTRY_IMAGE/${DEQP_RUNNER_IMAGE_PATH}:${DEQP_RUNNER_IMAGE_TAG}"
    DEQP_RUNNER_UPSTREAM_IMAGE: "$CI_REGISTRY/$FDO_UPSTREAM_REPO/${DEQP_RUNNER_IMAGE_PATH}:${DEQP_RUNNER_IMAGE_TAG}"

windows_container:
  rules:
    - if: *fork
    - if: *merge-request
  stage: container
  timeout: 120m
  inherit:
    default: false
  tags:
    - windows
    - shell
    - "2022"
  extends:
    - .windows_docker
  script:
    - .\.gitlab-ci\windows\deqp_runner_container.ps1 $CI_REGISTRY $CI_REGISTRY_USER $CI_REGISTRY_PASSWORD $DEQP_RUNNER_IMAGE $DEQP_RUNNER_UPSTREAM_IMAGE Dockerfile

test-windows:
  rules:
    - if: *fork
    - if: *merge-request
  stage: build
  inherit:
    default: false
  extends:
    - .windows_docker
  tags:
    - windows
    - docker
    - "2022"
  image: "$DEQP_RUNNER_IMAGE"
  needs:
    - windows_container
  script:
    - cargo test -j4

upload:
  stage: upload
  image: curlimages/curl:latest
  rules:
    - if: *tag
  script:
    - |
      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file target/debian/${AMD64_DEB} "${PACKAGE_REGISTRY_URL}/${AMD64_DEB}"
    - |
      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file target/debian/${ARM32_DEB} "${PACKAGE_REGISTRY_URL}/${ARM32_DEB}"
    - |
      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file target/debian/${ARM64_DEB} "${PACKAGE_REGISTRY_URL}/${ARM64_DEB}"

release:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  rules:
    - if: *tag
  script:
    - |
      release-cli create --name "Release $CI_COMMIT_TAG" --tag-name $CI_COMMIT_TAG \
        --assets-link "{\"name\":\"${AMD64_DEB}\",\"url\":\"${PACKAGE_REGISTRY_URL}/${AMD64_DEB}\"}" \
        --assets-link "{\"name\":\"${ARM32_DEB}\",\"url\":\"${PACKAGE_REGISTRY_URL}/${ARM32_DEB}\"}" \
        --assets-link "{\"name\":\"${ARM64_DEB}\",\"url\":\"${PACKAGE_REGISTRY_URL}/${ARM64_DEB}\"}"
